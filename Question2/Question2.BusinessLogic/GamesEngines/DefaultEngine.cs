﻿using System.Collections.Generic;
using Question2.Core;
using Question2.Core.Models;
using Question2.Infrastructure;

namespace Question2.BusinessLogic.GamesEngines
{
    public class DefaultEngine : IGameEngine
    {
        private ICardGenerator _cardsGenerator;
        private IGameStatusCalculator _statusCalculator;

        public DefaultEngine(ICardGenerator cardsGenerator, IGameStatusCalculator statusCalculator)
        {
            _cardsGenerator = cardsGenerator;
            _statusCalculator = statusCalculator;
        }

        public GameResultModel Play()
        {
            var firstPlayerCard = _cardsGenerator.GetCard();
            var secondPlayerCard = _cardsGenerator.GetCard();

            var result = new GameResultModel
            {
                Status = _statusCalculator.GetStatus(firstPlayerCard, secondPlayerCard),
                FirstPlayerCards = new List<CardModel> { firstPlayerCard },
                SecondPlayerCards = new List<CardModel> { secondPlayerCard }
            };

            return result;
        }
    }
}
