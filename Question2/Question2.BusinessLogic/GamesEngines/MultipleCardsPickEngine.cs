﻿using System.Collections.Generic;
using Question2.Core;
using Question2.Core.Models;
using Question2.Infrastructure;

namespace Question2.BusinessLogic.GamesEngines
{
    public class MultipleCardsPickEngine : IGameEngine
    {
        private ICardGenerator _cardsGenerator;
        private IGameStatusCalculator _statusCalculator;

        public MultipleCardsPickEngine(ICardGenerator cardsGenerator, IGameStatusCalculator statusCalculator)
        {
            _cardsGenerator = cardsGenerator;
            _statusCalculator = statusCalculator;
        }

        public GameResultModel Play()
        {
            var result = new GameResultModel
            {
                Status = GameStatus.Tie,
                FirstPlayerCards = new List<CardModel>(),
                SecondPlayerCards = new List<CardModel>()
            };

            while (result.Status == GameStatus.Tie)
            {
                var firstPlayerCard = _cardsGenerator.GetCard();
                var secondPlayerCard = _cardsGenerator.GetCard();

                result.Status = _statusCalculator.GetStatus(firstPlayerCard, secondPlayerCard);
                result.FirstPlayerCards.Add(firstPlayerCard);
                result.SecondPlayerCards.Add(secondPlayerCard);
            }
            
            return result;
        }
    }
}
