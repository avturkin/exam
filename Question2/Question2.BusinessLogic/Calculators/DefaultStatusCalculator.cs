﻿using Question2.Core;
using Question2.Infrastructure;

namespace Question2.BusinessLogic.Calculators
{
    public class DefaultStatusCalculator : IGameStatusCalculator
    {
        public GameStatus GetStatus(CardModel firstPlayerCard, CardModel secondPlayerCard)
        {
            return firstPlayerCard.Weight >= secondPlayerCard.Weight ? GameStatus.FirstPlayerWin : GameStatus.SecondPlayerWin;
        }
    }
}
