﻿using Question2.Core;
using Question2.Infrastructure;

namespace Question2.BusinessLogic.Calculators
{
    public class WithTiesStatusCalculator : IGameStatusCalculator
    {
        public GameStatus GetStatus(CardModel firstPlayerCard, CardModel secondPlayerCard)
        {
            if (firstPlayerCard.Weight == secondPlayerCard.Weight)
                return GameStatus.Tie;

            return firstPlayerCard.Weight > secondPlayerCard.Weight ? GameStatus.FirstPlayerWin : GameStatus.SecondPlayerWin;
        }
    }
}
