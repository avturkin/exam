﻿using Question2.Core;
using Question2.Infrastructure;

namespace Question2.BusinessLogic.Calculators
{
    public class WithTiesAndSuitsCalculator : IGameStatusCalculator
    {
        public GameStatus GetStatus(CardModel firstPlayerCard, CardModel secondPlayerCard)
        {
            if (firstPlayerCard.Weight == secondPlayerCard.Weight)
            {
                if ((byte)firstPlayerCard.Suit == (byte)secondPlayerCard.Suit)
                    return GameStatus.Tie;

                return (byte)firstPlayerCard.Suit > (byte)secondPlayerCard.Suit ? GameStatus.FirstPlayerWin : GameStatus.SecondPlayerWin;
            }

            return firstPlayerCard.Weight > secondPlayerCard.Weight ? GameStatus.FirstPlayerWin : GameStatus.SecondPlayerWin;
        }
    }
}
