﻿using Question2.Core;

namespace Question2.Infrastructure
{
    public interface ICardGenerator
    {
        CardModel GetCard();
    }
}
