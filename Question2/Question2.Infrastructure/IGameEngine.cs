﻿using Question2.Core.Models;

namespace Question2.Infrastructure
{
    public interface IGameEngine
    {
        GameResultModel Play();
    }
}
