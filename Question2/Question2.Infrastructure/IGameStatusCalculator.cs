﻿using Question2.Core;

namespace Question2.Infrastructure
{
    public interface IGameStatusCalculator
    {
        GameStatus GetStatus(CardModel firstPlayerCard, CardModel secondPlayerCard);
    }
}
