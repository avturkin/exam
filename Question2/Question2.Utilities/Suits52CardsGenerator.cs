﻿using System;
using Question2.Core;
using Question2.Infrastructure;

namespace Question2.Utilities
{
    public class Suits52CardsGenerator : ICardGenerator
    {
        public CardModel GetCard()
        {
            Random rnd = new Random();
            
            var weight = rnd.Next() % 52 + 1;
            var suit =  (CardSuit)rnd.Next(1, 5);
            
            return new CardModel(suit, weight);
        }
    }
}
