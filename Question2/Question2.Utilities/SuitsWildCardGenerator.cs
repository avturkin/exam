﻿using System;
using Question2.Core;
using Question2.Infrastructure;

namespace Question2.Utilities
{
    public class SuitsWildCardGenerator : ICardGenerator
    {
        private bool _hasWildCard = false;

        public CardModel GetCard()
        {
            Random rnd = new Random();

            var weight = rnd.Next() % 52 + 1;
            var suit = (CardSuit)rnd.Next(1, 5);

            if (!_hasWildCard)
            {
                if (rnd.Next(5) == 1)
                {
                    _hasWildCard = true;
                    weight = 100;
                    suit = CardSuit.WildCard;
                }
            }

            return new CardModel(suit, weight);
        }
    }
}
