﻿using System;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Question2.BusinessLogic.Calculators;
using Question2.BusinessLogic.GamesEngines;
using Question2.Core;
using Question2.Core.Models;
using Question2.Infrastructure;
using Question2.Utilities;

namespace Question2
{
    class Program
    {
        static void Main(string[] args)
        {
            Default();
            WithTies();
            WithTiesAndSuits();
            MultipleCardsPickGame();
            MultipleDecks();
            WildCard();
        }

        /// <summary>
        /// Default game
        ///
        /// Standard 52 card generator
        /// Not supports ties
        /// </summary>
        private static void Default()
        {
            var serviceProvider = new ServiceCollection()
                .AddSingleton<ICardGenerator, Suits52CardsGenerator>()
                .AddScoped<IGameStatusCalculator, DefaultStatusCalculator>()
                .AddSingleton<IGameEngine, DefaultEngine>()
                .BuildServiceProvider();

            var engine = serviceProvider.GetService<IGameEngine>();

            var result = engine.Play();
            ShowResult(result);
        }

        /// <summary>
        /// With ties game
        ///
        /// Standard 52 card generator
        /// Supports ties without suites
        /// </summary>
        private static void WithTies()
        {
            var serviceProvider = new ServiceCollection()
                .AddSingleton<ICardGenerator, Suits52CardsGenerator>()
                .AddScoped<IGameStatusCalculator, WithTiesStatusCalculator>()
                .AddSingleton<IGameEngine, DefaultEngine>()
                .BuildServiceProvider();

            var engine = serviceProvider.GetService<IGameEngine>();

            var result = engine.Play();
            ShowResult(result);
        }

        /// <summary>
        /// With ties game
        ///
        /// Standard 52 card generator
        /// Supports ties by weight and suits
        /// </summary>
        private static void WithTiesAndSuits()
        {
            var serviceProvider = new ServiceCollection()
                .AddSingleton<ICardGenerator, Suits52CardsGenerator>()
                .AddScoped<IGameStatusCalculator, WithTiesAndSuitsCalculator>()
                .AddSingleton<IGameEngine, DefaultEngine>()
                .BuildServiceProvider();

            var engine = serviceProvider.GetService<IGameEngine>();

            var result = engine.Play();
            ShowResult(result);
        }

        /// <summary>
        /// Multiple cards pick game
        ///
        /// Standard 52 card generator
        /// Supports ties by weight and suits
        /// </summary>
        private static void MultipleCardsPickGame()
        {
            var serviceProvider = new ServiceCollection()
                .AddSingleton<ICardGenerator, Suits52CardsGenerator>()
                .AddScoped<IGameStatusCalculator, WithTiesAndSuitsCalculator>()
                .AddSingleton<IGameEngine, MultipleCardsPickEngine>()
                .BuildServiceProvider();

            var engine = serviceProvider.GetService<IGameEngine>();

            var result = engine.Play();
            ShowResult(result);
        }

        /// <summary>
        /// Multiple desks game
        ///
        /// Standard 52 card generator
        /// Supports ties by weight and suits
        /// </summary>
        private static void MultipleDecks()
        {
            var serviceProvider = new ServiceCollection()
                .AddSingleton<ICardGenerator, Suits52CardsGenerator>()
                .AddScoped<IGameStatusCalculator, DefaultStatusCalculator>()
                .AddTransient<IGameEngine, MultipleCardsPickEngine>()
                .BuildServiceProvider();

            var desc1 = serviceProvider.GetService<IGameEngine>();
            var desc2 = serviceProvider.GetService<IGameEngine>();
            var desc3 = serviceProvider.GetService<IGameEngine>();
            var desc4 = serviceProvider.GetService<IGameEngine>();

            var result1 = desc1.Play();
            var result2 = desc2.Play();
            var result3 = desc3.Play();
            var result4 = desc4.Play();

            Console.WriteLine("<<<Desc 1>>>");
            ShowResult(result1);
            
            Console.WriteLine("<<<Desc 2>>>");
            ShowResult(result2);

            Console.WriteLine("<<<Desc 3>>>");
            ShowResult(result3);

            Console.WriteLine("<<<Desc 4>>>");
            ShowResult(result4);
        }


        /// <summary>
        /// Multiple cards pick game + wild card generator
        ///
        /// Wild-Card cards generator
        /// Supports ties by weight and suits
        /// </summary>
        private static void WildCard()
        {
            var serviceProvider = new ServiceCollection()
                .AddScoped<ICardGenerator, SuitsWildCardGenerator>()
                .AddSingleton<IGameStatusCalculator, WithTiesAndSuitsCalculator>()
                .AddTransient<IGameEngine, MultipleCardsPickEngine>()
                .BuildServiceProvider();

            var engine = serviceProvider.GetService<IGameEngine>();

            var result = engine.Play();
            ShowResult(result);
        }



        private static void ShowResult(GameResultModel result)
        {
            Console.WriteLine("First player cards: {0}", string.Join(", ", result.FirstPlayerCards.Select(c => $"{c.Weight} {c.Suit}")));
            Console.WriteLine("Second player cards: {0}", string.Join(", ", result.SecondPlayerCards.Select(c => $"{c.Weight} {c.Suit}")));

            var winner = result.Status == GameStatus.Tie 
                ? "Tie"
                : result.Status == GameStatus.FirstPlayerWin ? "First player" : "Second player";
            
            Console.WriteLine("Winner: {0}", winner); 
            Console.WriteLine("-----------------");
        }
    }
}
