﻿namespace Question2.Core
{
    public enum GameStatus
    {
        FirstPlayerWin = 1,
        SecondPlayerWin = 2,
        Tie
    }
}
