﻿namespace Question2.Core
{
    public enum CardSuit : byte
    {
        Spades = 1,
        Clubs = 2,
        Hearts = 3,
        Diamonds = 4,
        WildCard
    }
}
