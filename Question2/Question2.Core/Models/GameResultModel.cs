﻿using System.Collections.Generic;

namespace Question2.Core.Models
{
    public class GameResultModel
    {
        public GameStatus Status { get; set; }

        public List<CardModel> FirstPlayerCards { get; set; }

        public List<CardModel> SecondPlayerCards { get; set; }
    }
}
