﻿namespace Question2.Core
{
    public class CardModel
    {
        public CardModel(CardSuit suit, int weight)
        {
            Suit = suit;
            Weight = weight;
        }

        public CardSuit Suit { get; }

        public int Weight { get; }
    }
}
