﻿using System;
using Question1.Infrastructure;

namespace Question1.Security
{
    public class CryptoLengthCalculator : ICryptoLengthCalculator
    {
        public int GetEncodeLength(string input)
        {
            int l = input.Length;

            return (l / 3 + (Convert.ToBoolean(l % 3) ? 1 : 0)) * 4;
        }

        public int GetDecodedLength(string input)
        {
            int l = input.Length;

            return (l / 4 + ((Convert.ToBoolean(l % 4)) ? 1 : 0)) * 3;
        }
    }
}
