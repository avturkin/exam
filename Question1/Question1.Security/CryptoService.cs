﻿using System;
using Question1.Infrastructure;

namespace Question1.Security
{
    public class CryptoService : ICryptoService
    {
        private ITranscoder  _transcoder;
        private ICryptoLengthCalculator _lengthCalculator; 

        public CryptoService(ITranscoder transcoder, ICryptoLengthCalculator cryptoLengthCalculator)
        {
            _transcoder = transcoder;
            _lengthCalculator = cryptoLengthCalculator;
        }

        public string Encode(string input)
        {
            var transcode = _transcoder.GetCode();
            
            int l = input.Length;
            int cb = _lengthCalculator.GetEncodeLength(input);

            char[] output = new char[cb];
            for (int i = 0; i < cb; i++)
            {
                output[i] = '=';
            }

            int c = 0;
            int reflex = 0;
            const int s = 0x3f;

            for (int j = 0; j < l; j++)
            {
                reflex <<= 8;
                reflex &= 0x00ffff00;
                reflex += input[j];

                int x = ((j % 3) + 1) * 2;
                int mask = s << x;
                while (mask >= s)
                {
                    int pivot = (reflex & mask) >> x;
                    output[c++] = transcode[pivot];
                    int invert = ~mask;
                    reflex &= invert;
                    mask >>= 6;
                    x -= 6;
                }
            }

            switch (l % 3)
            {
                case 1:
                    reflex <<= 4;
                    output[c++] = transcode[reflex];
                    break;
                case 2:
                    reflex <<= 2;
                    output[c++] = transcode[reflex];
                    break;

            }
            Console.WriteLine("{0} --> {1}\n", input, new string(output));
            return new string(output);
        }


        public string Decode(string input)
        {
            int l = input.Length;
            int cb = _lengthCalculator.GetDecodedLength(input);

            char[] output = new char[cb];
            int c = 0;
            int bits = 0;
            int reflex = 0;
            for (int j = 0; j < l; j++)
            {
                reflex <<= 6;
                bits += 6;
                bool fTerminate = ('=' == input[j]);
                if (!fTerminate)
                    reflex += _transcoder.IndexOf(input[j]);
                
                while (bits >= 8)
                {
                    int mask = 0x000000ff << (bits % 8);
                    output[c++] = (char)((reflex & mask) >> (bits % 8));
                    int invert = ~mask;
                    reflex &= invert;
                    bits -= 8;
                }

                if (fTerminate)
                    break;
            }
            Console.WriteLine("{0} --> {1}\n", input, new string(output));
            return new string(output);
        }
    }
}
