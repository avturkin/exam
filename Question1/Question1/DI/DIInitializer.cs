﻿using System;
using Microsoft.Extensions.DependencyInjection;
using Question1.Infrastructure;
using Question1.Security;
using Question1.Utilities;

namespace Question1.DI
{
    public static class DIInitializer
    {
        public static IServiceProvider Init()
        {
            //setup our DI
            var serviceProvider = new ServiceCollection()
                .AddSingleton<ITranscoder, Transcoder>()
                .AddSingleton<ICryptoService, CryptoService>()
                .AddSingleton<ICryptoLengthCalculator, CryptoLengthCalculator>()
                .BuildServiceProvider();

            return serviceProvider;
        }
    }
}
