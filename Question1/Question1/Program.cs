﻿using System;
using Microsoft.Extensions.DependencyInjection;
using Question1.DI;
using Question1.Infrastructure;

namespace Question1
{
    class Program
    {
        static void Main(string[] args)
        {
            var serviceProvider = DIInitializer.Init();

            string test_string = "This is a test string";

            var cryptoService = serviceProvider.GetService<ICryptoService>();
            var cryptoString = cryptoService.Decode(cryptoService.Encode(test_string));

            if (Convert.ToBoolean(string.Equals(test_string, cryptoString)))
                Console.WriteLine("Test succeeded");
            else 
                Console.WriteLine("Test failed");
        }
    }
}
