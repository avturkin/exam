﻿using Question1.Infrastructure;

namespace Question1.Utilities
{
    public class Transcoder : ITranscoder
    {
        static char[] transcode = new char[65];

        public Transcoder()
        {
            for (int i = 0; i < 62; i++)
            {
                transcode[i] = (char)((int)'A' + i);
                if (i > 25) transcode[i] = (char)((int)transcode[i] + 6);
                if (i > 51) transcode[i] = (char)((int)transcode[i] - 0x4b);
            }
            transcode[62] = '+';
            transcode[63] = '/';
            transcode[64] = '=';
        }

        public char[] GetCode()
        {
            return transcode;
        }

        public int IndexOf(char ch)
        {
            int index;
            for (index = 0; index < transcode.Length; index++)
                if (ch == transcode[index])
                    break;
            return index;
        }
    }
}
