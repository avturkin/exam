﻿namespace Question1.Infrastructure
{
    public interface ITranscoder
    {
        char[] GetCode();

        int IndexOf(char ch);
    }
}
