﻿namespace Question1.Infrastructure
{
    public interface ICryptoService
    {
        string Encode(string input);

        string Decode(string input);
    }
}
