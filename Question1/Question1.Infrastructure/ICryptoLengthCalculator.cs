﻿namespace Question1.Infrastructure
{
    public interface ICryptoLengthCalculator
    {
        int GetEncodeLength(string input);

        int GetDecodedLength(string input);
    }
}
