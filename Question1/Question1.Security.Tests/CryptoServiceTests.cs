﻿using System;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Question1.Infrastructure;
using Question1.Utilities;

namespace Question1.Security.Tests
{
    [TestClass]
    public class CryptoServiceTests
    {
        private IServiceProvider _container;

        [TestInitialize]
        public void Setup()
        {
            //setup our DI
            _container = new ServiceCollection()
                .AddSingleton<ITranscoder, Transcoder>()
                .AddSingleton<ICryptoService, CryptoService>()
                .AddSingleton<ICryptoLengthCalculator, CryptoLengthCalculator>()
                .BuildServiceProvider();
        }

        /// <summary>
        /// String doesn't change after encoding and decoding
        ///
        /// Setup:
        ///     No empty string
        ///
        /// Expected result:
        ///     The string is the same after the Encoding and Decoding operations
        /// </summary>
        [TestMethod]
        public void EncodeDecode_NotEmptyString_StringTheSameAfterEncodingAndDecoding()
        {
            string test_string = "This is a test string";
            
            var cryptoService = _container.GetService<ICryptoService>();
            var cryptoString = cryptoService.Decode(cryptoService.Encode(test_string));

            Assert.AreEqual(test_string, cryptoString);
        }
    }
}
