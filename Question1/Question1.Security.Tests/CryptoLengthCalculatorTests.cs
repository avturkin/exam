using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Question1.Security.Tests
{
    [TestClass]
    public class CryptoLengthCalculatorTests
    {
        /// <summary>
        /// Get length for the string after the encoding  
        /// 
        /// Setup
        /// Not empty string before the encoding
        ///
        ///  Expected result:
        ///     Encoded length string = 28
        /// </summary>
        [TestMethod]
        public void GetEncodeLength_NotEmptyString_Length28()
        {
            var lengthCalculator = new CryptoLengthCalculator();

            var data = "This is a test string";
            var length = lengthCalculator.GetEncodeLength(data);

            Assert.AreEqual(28, length);
        }

        /// <summary>
        /// Get length for the string before the encoding  
        /// 
        /// Setup
        /// Not empty encoded string
        ///
        ///  Expected result:
        ///     String decoded length is the same as before encoding
        /// </summary>
        [TestMethod]
        public void GetDecodeLength_NotEncodedString_LengthTheSameAsBefore()
        {
            var lengthCalculator = new CryptoLengthCalculator();

            var beforeEncoding = "This is a test string";
            var afterEncoding = "VGhpcyBpcyBhIHRlc3Qgc3RyaW5n";

            var length = lengthCalculator.GetDecodedLength(afterEncoding);

            Assert.AreEqual(beforeEncoding.Length, length);
        }
    }
}
